### Intro

This is a starter project for an Angular project using TypeScript, including 

- Angular modules, components, and services
- Sass/scss support
- Local dev server with hot module replacement. Modules are rebuilt and reloaded on save for instant updates during development.
- Unit testing with Karma and Jasmine. Tests can be rerun on save.
- Unit test code coverage
- End-to-End testing framework (e2e)
- Production build
- Webpack 2 for bundling and module loading
- Folder structure
- File naming conventions
- TS Lint to aid in TypeScript best practices
- Codelyzer to aid in Angular best practices

### Intial setup
Install [node](https://nodejs.org/en/).

Install local node dependencies.
```
$ cd /path/to/project
$ npm install
```

### Build and run
Start the webpack dev server (node script from package.json).
```
$ npm start
```
Start the app (simple page displaying Hello DAT) - browse to
```
localhost:3100
```
Changes to source files will automatically be rebuilt and displayed in the browser while the dev server is running.

Use Ctrl-C to stop the dev server.

### Unit tests
***For a single run***

Run unit tests with a node script command.
```
$ npm run test
```
Run unit tests with code coverage. 

A coverage folder is created that contains the code coverage results in html and json formats.
A coverage summary is written to the terminal on each run. 
The coverage folder is deleted before each coverage run.
```
$ npm run test-cover
```
Command                     | Report Output
:---------------------------| :--------------------------------------------------
`$ npm run cover-report`    | Writes coverage statistics to the terminal.
`$ npm run cover-report:mac`| Shows the html coverage report in Chrome on a Mac.
`$ npm run cover-report:win`| Shows the html coverage report in Chrome on Windows.


***To keep Karma running and watch for changes***

This keeps the browser open for debugging tests with the browser devtools.
```
$ npm run test-watch
```
With code coverage.


### End-to-End tests
Start the node and Selenium servers in a terminal instance:
```
$ npm run e2e-start
```
Then, too run the e2e tests, create a **second terminal instance**.

***For a single run***

```
$ npm run e2e
```
***For interactive REPL shell***

Puts protractor in read-eval-print loop / command-line mode to enter protractor commands.
```
$ npm run e2e-watch
```


### Production build
```
$ npm run build
```
This command will create bundles and hashed files for production deployment. Output files are placed in the 'dist' folder.

### Things to add
These capabilities are not yet in the template. Feel free to add them as you can.

- ahead-of-time compiling (aot) for smaller downloads and faster browser startup


### Problems:
Headless unit testing is not working with PhantomJS on mac. Timeout error connecting localhost:9876.

```
Config:
    package.json
        "karma-phantomjs-launcher": "^1.0.2"
    config/karma.config.js
        browsers: ['PhantomJS'],
```

Using Chrome for unit tests works.

```
Config:
    package.json
        "karma-chrome-launcher": "^2.0.0",
    config/karma.config.js
        browsers: ['Chrome'],
```


### Who do I talk to? ###

* Greg Larson is the repository owner. greg.larson@dat.com
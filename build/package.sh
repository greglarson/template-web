#! /bin/bash
set -e
set -x

this_dir="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
cd $this_dir/..

if [[ -z "$1" ]]; then
    echo "Must supply version number (ex. v1.2.3-alpha.4)" 1>&2
    exit 1
fi

version=$1

name=$(jq -r '.name' package.json)

jq -r '.version |= "'"$version"'"' package.json > tmp.json

mv package.json package.json.tmp
mv tmp.json package.json

npm install
npm prune
npm shrinkwrap

npm run build

npm pack

mv package.json.tmp package.json
rm npm-shrinkwrap.json

mv $name-$version.tgz build/artifacts

exit 0

#! /bin/bash
set -e
set -x

# make the current directory the root of the node app
this_dir="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
cd $this_dir/..

export DAT_CONFIG=/config/test.json
export AWS_PROFILE=dat
timeout_ms=5000

npm run test-build

exit 0

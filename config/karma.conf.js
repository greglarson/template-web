var webpackConfig = require('./webpack.test');

module.exports = function (config) {

  // add coverage if the cover flag is set in the command
  const coverage = config.cover ? ['coverage'] : [];

  var _config = {
    basePath: '',

    frameworks: ['jasmine'],

    customLaunchers: {
      chrome_with_debugging: {
        base: 'Chrome',
        flags: ['--remote-debugging-port=9222']
      }
    },
    
    // todo: remove inclusion of auth0.js once it is successfully imported in angular modules via npm package
    files: [
      'https://cdn.auth0.com/js/auth0/8.7/auth0.min.js',
      { pattern: './config/karma-test-shim.js', included: true, watched: false }
    ],

    preprocessors: {
      './config/karma-test-shim.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      stats: 'errors-only'
    },

    webpackServer: {
      noInfo: true
    },

    coverageReporter: {
      type: 'in-memory'
    },

    remapCoverageReporter: {
      'text-summary': null,
      json: './coverage/coverage.json',
      html: './coverage/html'
    },

    // Use maxLogLines to control size of error stack traces
    specReporter: {
      maxLogLines: 3
    },

    reporters: ['spec']
      .concat(coverage)
      .concat(coverage.length > 0 ? ['remap-coverage'] : []),
    
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['Chrome'],
    singleRun: true
  };

  config.set(_config);
};

var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');

module.exports = {
  entry: {
    'polyfills': './src/polyfills.ts',
    'vendor': './src/vendor.ts',
    'app': './src/main.ts'
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          { loader: 'awesome-typescript-loader?tsconfig=tsconfig.json' }, 
          { loader: 'angular2-template-loader' }
        ]
      },
      {
        test: /\.html$/,
        use: { loader: 'html-loader' }
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        use: { loader: 'file-loader?name=assets/[name].[hash].[ext]' }
      },
      {
        // converts component scss stylesUrl to inline string in the js file
        test: /\.scss$/,
        include: helpers.root('src', 'app'),
        use: [
          { loader: 'raw-loader' },
          { loader: 'sass-loader' }
        ]
      },
	{
        test: /\.scss$/,
        exclude: helpers.root('src', 'app'),
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
		  use: ['css-loader', 'sass-loader']
        })
      }
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jquery: "jquery",
      "window.jQuery": "jquery",
      jQuery:"jquery"
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    //   favicon: 'public/images/favicon.ico'
    }),
    // This fixes angular es5 warning during wepack startup:
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)@angular/,
      helpers.root('./src'),
      {}
    )
  ]
};

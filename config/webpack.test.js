var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

const ENV = process.env.NODE_ENV = process.env.ENV = 'test';
const autowatch = process.env.npm_lifecycle_script.indexOf('--auto-watch') !== -1;

module.exports = webpackMerge(commonConfig, {
  devtool: 'inline-source-map',

  output: {},

  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new ExtractTextPlugin('[name].css'),
    new webpack.DefinePlugin({
      'process.env': {
        'ENV': JSON.stringify(ENV)
      }
    })
  ],
  module: {
    // instrument for code coverage only if not auto-watching
    // ts source files do not load properly for debugging with this plugin
    rules: !autowatch 
      ? [
          {
            enforce: 'post',
            test: /\.(ts)$/,
            loader: 'istanbul-instrumenter-loader',
            include: helpers.root('src', 'app'),
            exclude: [
              /\.(e2e|spec)\.ts$/,
              /node_modules/
            ]
          }  
      ] 
    : []
  },
  stats: { colors: true, reasons: true }
 
});

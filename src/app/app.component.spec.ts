/* tslint:disable:no-unused-variable */

import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ResourceService } from './services/resource.service';
import { AuthService } from './services/auth.service';

describe('App', () => {
  let resourceService: ResourceService;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule(
      {
        imports: [ RouterTestingModule ],
        declarations: [ AppComponent ],
        providers: [ ResourceService, AuthService ]
      });

    resourceService = TestBed.get(ResourceService);
    authService = TestBed.get(AuthService);
  });

  it ('should create AppComponent', () => {
    let fixture = TestBed.createComponent(AppComponent);
    expect(fixture.componentInstance instanceof AppComponent).toBe(true);
  });
  it('should have proper title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual(ResourceService.application.title);
  });
});

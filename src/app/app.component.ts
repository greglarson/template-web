import { Component } from '@angular/core';

import '../../public/sass/styles.scss';
import { AuthService } from './services/auth.service';
import { ResourceService } from './services/resource.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private htmlText: any;
  private title: string;
  private homeMenu: string;
  private nonsecureMenu: string;
  private secureMenu: string;
  private loginButton: string;
  private logoutButton: string;

  constructor(private authService: AuthService) {
    this.setMenuItems();
    this.authService.scheduleRenewal(window.location.pathname);
  }

  private setMenuItems(): void {
    this.htmlText = ResourceService.application;
    this.title = ResourceService.application.title;
    this.homeMenu = ResourceService.nav.homeMenu;
    this.nonsecureMenu = ResourceService.nav.nonSecureMenu;
    this.secureMenu = ResourceService.nav.secureMenu;
    this.loginButton = ResourceService.nav.loginButton;
    this.logoutButton = ResourceService.nav.logoutButton;
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';

import { AUTH_PROVIDERS, provideAuth } from 'angular2-jwt';
import { AuthService } from './services/auth.service';
import { AuthGuard} from './services/auth.guard.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AuthCallbackComponent } from './authcallback.component';
import { SecureComponent } from './secure/secure.component';
import { NonsecureComponent } from './nonsecure/nonsecure.component';
import { RoutingModule } from './routing.module';
import { ResourceService } from './services/resource.service';

@NgModule({
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule,
    RoutingModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    AuthCallbackComponent,
    SecureComponent,
    NonsecureComponent
  ],
  providers: [
    ResourceService,
    AuthService,
    AuthGuard,
    AUTH_PROVIDERS,
    provideAuth({
      tokenName: 'access_token',
      globalHeaders: [{ 'Content-Type': 'application/json' }]
    })
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

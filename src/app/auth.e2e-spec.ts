// Include these imports in e2e tests.
import { browser, by, element } from 'protractor';
import {} from 'jasmine';
import 'reflect-metadata';

import { ResourceService } from './services/resource.service';

describe('Auth e2e tests - not logged in', function () {

    beforeEach(function () {
    });

    // TODO: check with Greg Larson to see if these jasmine tests can be compiled successfully once all NPM
    // packages have been brought up to latest (with Angular4)
    /*
    it('home page should not be logged in', () => {
        browser.get('/');
        //expect(element(by.css('my-app main h4')).getText()).toEqual(ResourceService.sessionMessages.notAuthenticated);
        let navElement = element(by.buttonText(ResourceService.nav.loginButton));
        expect(navElement.isPresent()).toBeTruthy();
    });

    it('Non-secure page should not be logged in', () => {
        browser.get('/nonsecure');
        expect(element(by.css('my-app main h4')).getText()).toEqual(ResourceService.nonSecureComponent.mainMessageAnonymous);
    });

    it('secure page should not be logged in', () => {
        browser.get('/secure');
        // user is taken back to home route / component
        expect(element(by.css('my-app main h4')).getText()).toEqual(ResourceService.sessionMessages.notAuthenticated);
    });

    it('home page should show correct menu items', () => {
        browser.get('/');
        let navElement = element(by.linkText(ResourceService.nav.homeMenu));
        expect(navElement.isPresent()).toBeTruthy();

        navElement = element(by.linkText(ResourceService.nav.nonSecureMenu));
        expect(navElement.isPresent()).toBeTruthy();

        navElement = element(by.linkText(ResourceService.nav.secureMenu));
        expect(navElement.isPresent()).toBeFalsy();
    });
    */

});

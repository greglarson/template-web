import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'my-callback',
  template: ``
})
export class AuthCallbackComponent implements OnInit {
  // todo: this is being called when the app is loading and when /callback is GET'd
  //  ... and thus is being called twice, in rapid succession, after login
  // Token renewal is working 'fine' on the other hand

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.authService.handleAuthentication();
    this.authService.scheduleRenewal(window.location.pathname);
  }
}

interface AuthConfiguration {
    CLIENT_ID: string;
    CLIENT_DOMAIN: string;
    AUDIENCE: string;
    SCOPE: string;
    RESPONSE_TYPE: string;
    RENEW_URI: string;
    LOGIN_COMMANDS: any[];
}

/*
export const myConfig: AuthConfiguration = {
    CLIENT_ID: "A9IDX8pp1PYn9eIWHVFBP9TTYg8n8G1E",
    CLIENT_DOMAIN: "dat-dev.auth0.com",
    AUDIENCE: "https://dev-api.dat.com",
    SCOPE: "openid profile",
    RESPONSE_TYPE: "token id_token",
    RENEW_URI: "http://localhost:3100/silent.html?v=3",
    LOGIN_COMMANDS: [''],
};
*/

export const myConfig: AuthConfiguration = {
    CLIENT_ID: 'jYKcRT8aex4gdFUXc3axT494dgWj9POh',
    CLIENT_DOMAIN: 'dat-trial.auth0.com',
    AUDIENCE: 'https://datapi.test.com/',
    SCOPE: 'openid profile',
    RESPONSE_TYPE: 'token id_token',
    RENEW_URI: 'http://localhost:3100/silent.html?v=3',
    LOGIN_COMMANDS: [''],
};

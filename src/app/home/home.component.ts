import { Component }  from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ResourceService } from '../services/resource.service';

@Component({
    selector: 'my-home',
    templateUrl: './home.component.html'
})

export class HomeComponent {
    private aboutTokenMessage: string;

    constructor(private authService: AuthService) {
        this.aboutTokenMessage = ResourceService.sessionMessages.aboutYourToken;
    }

    private get mainMessage(): string {
        if (this.authService.isAuthenticated()) {
            return ResourceService.sessionMessages.authenticated;
        } else {
            return ResourceService.sessionMessages.notAuthenticated;
        }
    }

    private get expiresAt(): string {
        return JSON.parse(localStorage.getItem('expires_at'));
    }
}

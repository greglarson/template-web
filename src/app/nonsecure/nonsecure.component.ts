import { Component }  from '@angular/core';
import { Http }       from '@angular/http';
import { AuthService }       from '../services/auth.service';
import { ResourceService } from '../services/resource.service';

const baseApi: string = 'https://api.dat-apps.com/template/';

@Component({
    selector: 'my-nonsecure',
    templateUrl: './nonsecure.component.html'
})

export class NonsecureComponent {
    private callApiText: string;
    private apiResponseText: string;
    private response: string;

    constructor(private authService: AuthService, private http: Http) {
        this.callApiText = ResourceService.nonSecureComponent.callApi;
        this.apiResponseText = ResourceService.nonSecureComponent.apiResponse;
    }

    public callApi(): void {
    this.http.get(baseApi + 'auth/anonMessage')
        .subscribe(
            (data: any) => { this.response = data.text(); },
            (error: string) => { console.log(error); }
        );
    }

    private get mainMessage(): string {
        if (this.authService.isAuthenticated()) {
            return ResourceService.nonSecureComponent.mainMessageKnown;
        } else {
            return ResourceService.nonSecureComponent.mainMessageAnonymous;
        }
    }
}

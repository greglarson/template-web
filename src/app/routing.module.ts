import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './services/auth.guard.service';

import { HomeComponent } from './home/home.component';
import { SecureComponent } from './secure/secure.component';
import { NonsecureComponent } from './nonsecure/nonsecure.component';
import { AuthCallbackComponent } from './authcallback.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    {
        path: 'secure',
        component: SecureComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'nonsecure',
        component: NonsecureComponent
    },
    {
        path: 'callback',
        component: AuthCallbackComponent
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class RoutingModule {}

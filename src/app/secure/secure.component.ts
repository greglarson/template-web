import { Component }  from '@angular/core';
import { AuthHttp }   from 'angular2-jwt';
import { AuthService } from '../services/auth.service';
import { ResourceService } from '../services/resource.service';

const baseApi: string = 'https://api.dat-apps.com/template/';

@Component({
  selector: 'my-secure',
  templateUrl: './secure.component.html'
})

export class SecureComponent {
  private jwt: string;
  private response: string;
  private callApiText: string;
  private apiResponseText: string;

  constructor(private authService: AuthService, private authHttp: AuthHttp) {
    this.jwt = authService.getAccessToken();
    this.callApiText = ResourceService.secureComponent.callApi;
    this.apiResponseText = ResourceService.secureComponent.apiResponse;
  }

  public callSecureApi(): void {
    this.authHttp.get(baseApi + 'auth/userMessage')
        .subscribe(
            (data: any) => { this.response = data.text(); },
            (error: string) => { console.log(error); }
        );
    }

  private get mainMessage(): string {
        if (this.authService.isAuthenticated()) {
            return ResourceService.secureComponent.mainMessageKnown;
        } else {
            return ResourceService.secureComponent.mainMessageAnonymous;
        }
    }

}

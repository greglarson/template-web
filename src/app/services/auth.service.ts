import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/timer';
import { myConfig } from '../configs/auth.config';

const ID_TOKEN: string = 'id_token';
const ACCESS_TOKEN: string = 'access_token';
const PROFILE: string = 'profile';
const TOKEN_EXPIRES_AT: string = 'expires_at';
const TOKEN_EXPIRES_BUFFER_SECONDS: number = 120;
const POST_MESSAGE_DATA_TYPE: string = 'dat-auth-result';
const CALLBACK_PATH: string = '/callback';

// todos
//  A promise must be used in fetching the user's profile so that subsequent code is forced to wait

// Avoid name not found warnings
declare var auth0: any;

@Injectable()
export class AuthService {
  refreshSubscription: any;

  auth0: any = new auth0.WebAuth({
    domain: myConfig.CLIENT_DOMAIN,
    clientID: myConfig.CLIENT_ID,
    redirectUri: window.location.origin + CALLBACK_PATH,
    responseType: myConfig.RESPONSE_TYPE,
    scope: myConfig.SCOPE,
    audience: myConfig.AUDIENCE
  });

  // Create a stream of logged in status to communicate throughout app
  loggedIn: boolean;
  loggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.loggedIn);

  constructor(private router: Router) {
    if (this.isAuthenticated()) {
      this.setLoggedIn(true);
    }
  }

  public handleAuthentication(): void {
    this.auth0.parseHash({ _idTokenVerification: true }, (err: any, authResult: any) => {
      if (err) {
        alert(`Error: ${err.errorDescription}`);
      }
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';

        this.setLoggedIn(true);
        this.setSession(authResult);
        this.fetchAndStoreProfile(authResult.accessToken);

        this.router.navigate(['']);
      }
    });
  }

  // calling the Auth0 hosted login page so that we can do SSO and use Implicit Grant to get JWT access_token
  public login(): void {
    this.auth0.authorize();
  }

  public isAuthenticated(): boolean {
    const expiresAt: any = JSON.parse(localStorage.getItem(TOKEN_EXPIRES_AT));
    if (expiresAt === null) { return false; }

    return Date.now() < expiresAt;
  }

  public renewToken(): void {
    this.auth0.renewAuth({
      audience: myConfig.AUDIENCE,
      redirectUri: myConfig.RENEW_URI,
      postMessageDataType: POST_MESSAGE_DATA_TYPE,
      usePostMessage: true
    }, (err: any, result: any) => {
      if (err) {
        if (this.isAuthenticated()) {
          console.log(`Could not get a new token using silent authentication (${err.error}).`);
        } else {
          console.log(`SSO check failed.`);
        }        
      } else if (result) {
         console.log(`Successfully renewed access token.`);
         this.setSession(result);
       }
    });
  }

  public logout(): void {
    localStorage.removeItem(ACCESS_TOKEN);
    localStorage.removeItem(ID_TOKEN);
    localStorage.removeItem(PROFILE);
    localStorage.removeItem(TOKEN_EXPIRES_AT);
    this.unscheduleRenewal();
    this.setLoggedIn(false);

    this.auth0.logout({
      returnTo: this.getLoginUrl(),
      clientID: myConfig.CLIENT_ID,
    });
  }

  public getAccessToken(): string {
    return localStorage.getItem(ACCESS_TOKEN) || '';
  }

  public getLoginCommands(): any[] {
    return myConfig.LOGIN_COMMANDS;
  }

  public getUserName(): string {
    let name: string;
    let profileString: string = localStorage.getItem(PROFILE) || '';

    if (profileString.length > 0) {
      let profileObj: any = JSON.parse(profileString);
      name = profileObj.nickname || '';
    }

    return name;
  }

  public scheduleRenewal(path?: string): void {
    if (!this.isAuthenticated()) {
      if (path && path.toLowerCase() === CALLBACK_PATH) {
        return;
      } else {
        return this.renewToken();
      }
    }

    this.unscheduleRenewal();

    const bufferTime: number = TOKEN_EXPIRES_BUFFER_SECONDS * 1000;
    const expiresAt: any = JSON.parse(localStorage.getItem(TOKEN_EXPIRES_AT));

    const source: Observable<any> = Observable.of(expiresAt).flatMap(
      (expires: any) => {
        const now: number = Date.now();

        // Use the delay in a timer to run the renew at the proper time
        return Observable.timer(Math.max(1, (expiresAt - bufferTime) - now));
      });

    this.refreshSubscription = source.subscribe(() => {
      this.renewToken();
    });
  }

  private getLoginUrl(): string {
    return window.location.origin + this.router.createUrlTree(this.getLoginCommands()).toString();
  }

  private unscheduleRenewal(): void {
    if (!this.refreshSubscription) {
        return;
    }
    this.refreshSubscription.unsubscribe();
  }

  private fetchAndStoreProfile(accessToken: string): void {
    this.auth0.client.userInfo(accessToken, (err: any, profile: any) => {
      localStorage.setItem(PROFILE, JSON.stringify(profile));
    });
  }

  private setLoggedIn(value: boolean): void {
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  private setSession(authResult: any): void {
    const tokenExpiresAt: any = JSON.stringify((authResult.expiresIn * 1000) + Date.now());

    localStorage.setItem(ACCESS_TOKEN, authResult.accessToken);
    localStorage.setItem(ID_TOKEN, authResult.idToken);
    localStorage.setItem(TOKEN_EXPIRES_AT, tokenExpiresAt);

    this.scheduleRenewal();
  }

}

import { Injectable } from '@angular/core';

// NOTE: return types flagged by tslint can be ignored in this file. Typing the string structs as 'any' breaks intellisense.
@Injectable()
export class ResourceService {

    public static application = {
        companyName: 'DAT',
        title: 'Angular 2 Template'
    };

    public static errors = {
        error1: 'Error 1.',
        error2: 'Error 2.',
    };

    public static auth = {
        notAuthenticated: 'YOU ARE NOT LOGGED IN',
        invalidSession: 'INVALID SESSION TOKEN',
        sessionExpired: 'SESSION EXPIRED'
    };

    public static sessionMessages = {
        notAuthenticated : 'You are not logged in, please click the \'Log in\' button to login.',
        authenticated: 'You are now logged in.',
        aboutYourToken: 'About your token:'
    };

    public static nav = {
        homeMenu: 'Home',
        nonSecureMenu: 'Non-secure area',
        secureMenu: 'Secure area',
        loginButton: 'Log In',
        logoutButton: 'Log Out'
    };

    public static nonSecureComponent = {
        mainMessageAnonymous: 'You are in the non-secure area and are not logged in.',
        mainMessageKnown: 'You are in the non-secure area, logged in as:',
        callApi: 'Call Non-secured API',
        apiResponse: 'The response of calling the API is:'
    };

    public static secureComponent = {
        mainMessageAnonymous: 'You are in the secure area but are not logged in.  You shouldn\'t be here!',
        mainMessageKnown: 'You are in the secure area, logged in as:',
        callApi: 'Call Secured API',
        apiResponse: 'The response of calling the API is:'
    };

    constructor() { }
}
